package com.ryan.demos.springcloudsleuthdemo.service2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@RestController
@RequestMapping("/api/service2")
public class TestController {

    private static final Logger LOG = LoggerFactory.getLogger(TestController.class);

    private final RestTemplate restTemplate;

    public TestController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping
    public String testEndpoint() throws InterruptedException {
        LOG.info("Request received!");
        Thread.sleep(new Random().nextInt(1000));
        String service3Response = restTemplate.getForObject("http://localhost:9102/api/service3", String.class);
        String service4Response = restTemplate.getForObject("http://localhost:9103/api/service4", String.class);
        return "[Service 2]-(" + service3Response + "-" + service4Response + ")";
    }

}
