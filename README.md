### Toggling Zipkin
By default, all of these Spring Boot apps have their Zipkin client disabled, since you might not be running a Zipkin server.
If you are running Zipkin and want to enable this functionality, modify the `application.yml` property file for each Spring Boot app,
and set `spring.zipkin.enabled: true`