package com.ryan.demos.springcloudsleuthdemo.service1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;


@RestController
@RequestMapping("/api/service1")
public class TestController {

    private static final Logger LOG = LoggerFactory.getLogger(TestController.class);

    private final RestTemplate restTemplate;

    public TestController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping
    public String testEndpoint() throws InterruptedException {
        LOG.info("Request received!");
        Thread.sleep(new Random().nextInt(1000));
        String response = restTemplate.getForObject("http://localhost:9101/api/service2", String.class);
        Thread.sleep(new Random().nextInt(1000));
        return "[Service 1]-" + response;
    }

}
